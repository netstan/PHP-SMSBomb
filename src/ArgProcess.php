<?php
/**
 * 参数处理类， 用户某些请求的参数计算等
 */

class ArgProcess{
	
	public static function md5_58($mobile, $timestamp) {
		$mobile = Helper::replace($mobile);
		$timestamp = Helper::replace($timestamp);
		
		return md5(md5($mobile).substr($timestamp, 5, 6));
	}
	
	
}