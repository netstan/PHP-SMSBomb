<?php
class Request{
	// 目标手机号码
	public static $mobile = '';
	
	/**
	 * 复合请求
	 */
	public static function complexRequest($data) {
		foreach($data['list'] as $item) {
			$item['referer'] = $data['referer'];
			$item['cookie'] = $data['cookie'];
			
			self::normalRequest($item);
		}
	}
	
	/**
	 * 匹配模式的 复合请求
	 * 匹配表单中的 值, 作为参数传递
	 */
	public static function matchComplexRequest($data) {
		$data['match']['cookie'] = $data['cookie'];
		$data['match']['referer'] = $data['referer'];
		$attributes = Helper::matchAttributes($data['match']);
		
		foreach($data['list'] as &$item) {
			$item['url'] = Helper::replace($item['url'], $attributes);
			$item['data'] = Helper::replace($item['data'], $attributes);
		}
		
		self::complexRequest($data);
	}
	
	/**
	 * 普通请求
	 */
	public static function normalRequest($data) {
		$requestData = Helper::replace($data['data']);
		$url = Helper::replace($data['url']);
		
		// 有需要处理的参数
		if(isset($data['process'])) {
			!empty($requestData) && $requestData .= '&';
			
			foreach($data['process'] as $key => $item) {
				$requestData .= sprintf('%s=%s&', $key, call_user_func_array($item[0], $item[1]));
			}
			$requestData = substr($requestData, 0, -1);
		}
		
		// 需要匹配指定url内容的参数
		if(isset($data['match'])) {
			$data['match']['cookie'] = @ $data['cookie'];
			$data['match']['referer'] = @ $data['referer'];
			$attributes = Helper::matchAttributes($data['match']);
			
			$data['url'] = Helper::replace($data['url'], $attributes);
			$requestData = Helper::replace($requestData, $attributes);
		}
		
		return self::sendRequest(
			$url,
			$requestData,
			array(
				'is_post'=> @ $data['post'],
				'referer'=> @ $data['referer'],
				'cookie'=> @ $data['cookie'],
				'ssl_verifypeer'=> isset($data['ssl_verifypeer']) ? $data['ssl_verifypeer'] : true,
				'is_log'=> isset($data['log']) ? $data['log'] : true,
				'http_header'=> isset($data['http_header']) ? $data['http_header'] : array()
			)
		);
	}
	
	/**
	 * 发送请求
	 */
	public static function sendRequest($url, $data, $params = array()) {
		$params = array_merge(array(
			'is_post'=>false,
			'referer'=>'',
			'cookie'=>'',
			'is_log'=> true,
			'ssl_verifypeer'=>true,
			'timeout'=>10
		), $params);
		
		$params['is_post'] ? $params['data'] = $data : $url .= '?'.$data;
		
		// 从URL 自动匹配 referer
		if(empty($params['referer'])) {
			$urlData = parse_url($url);
			$params['referer'] = $urlData['scheme'].$urlData['host'];
		}
		
		// 自动匹配 https
		$params['https'] = preg_match("#^https#", $url);
		
		$curl = Curl::model();
		
		// 设置 cookie
		if(!empty($params['cookie'])) {
			$curl->cookie = $params['cookie'];
			$params['use_cookie'] = true;
		}
		$result = $curl->request($url, $params);
		
		if(!$params['is_log']) return $result;
		
		$resultArr = @ json_decode($result);
		$resultArr && $result = $resultArr;
		
		$log = sprintf("\n [time: %s] \n url: %s \n data: %s \n result: %s\n ----end---- \n",
			date('Y-m-d H:i:s'), $url, var_export($data, true), var_export($result, true));
		Helper::log($log);
	}
	
}