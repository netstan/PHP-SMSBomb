###短信炸弹测试版
PHP >= 5.3

利用各个网站发送手机验证码的功能，给一手机号码发送多条短信

###使用
1. 命令行 php bomb.php --mobile=11111111111
2. 浏览器 www.***.com/bomb.php?mobile=11111111111

###数据配置
    <?php
    
    /**
     * 基础类型，
     * 其他类型也会相应地转换成基础类型
     */
    array(
         //请求URL地址
    	'url'=>'http://www.***.com/send',
		
         // 请求参数， 字符串
        'data'=>'mobile={$mobile}',
		
        // header 头的referer(可选, 默认从url匹配)
        'referer'=>'http://www.***.com',
        
        // 是否为post请求(可选，默认为false)
        'post'=> true,
        
         // (https 请求时有用, 设置curl的 CURLOPT_SSL_VERIFYPEER)
        'ssl_verifypeer' => false,
        
        // 需要计算处理的参数，键对应curl请求参数的键， 值对应 call_user_func_array 的两个参数
        'process'=> array(
    		'p'=>array(array('ArgProcess', 'md5_58'), array('{$mobile}', '{$millisecond}')),
		)
	),